#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>


#define Rel1 0
#define Rel2 2
#define Rel3 3
#define Rel4 4
#define _SEC 1000

const char *R1="R1";
const char *R2="R2";
const char *R3="R3";
const char *R4="R4";
const int NUMB_OF_RELAY = 4;
//const char *cmd;

void energizingRelay(char *relayNumber){
	if(strcmp(relayNumber,"R1")==0) digitalWrite(Rel1, HIGH);
	else if(strcmp(relayNumber,"R2")==0) digitalWrite(Rel2, HIGH);
	else if(strcmp(relayNumber,"R3")==0) digitalWrite(Rel3, HIGH);
	else if(strcmp(relayNumber,"R4")==0) digitalWrite(Rel4, HIGH);
	
	else{
		fprintf(stderr,"Please specify a proper relay number!\n");
	}
	
	
}

void deEnrgizeRelay(char *relayNumber){

	if(strcmp(relayNumber,"R1")==0) digitalWrite(Rel1, LOW);
	else if(strcmp(relayNumber,"R2")==0) digitalWrite(Rel2, LOW);
	else if(strcmp(relayNumber,"R3")==0) digitalWrite(Rel3, LOW);
	else if(strcmp(relayNumber,"R4")==0) digitalWrite(Rel4, LOW);
	
	else{
		fprintf(stderr,"Please specify a proper relay number!\n");
	}
	


}

void readingState(char *relayNumber){
	bool state;
	if(strcmp(relayNumber,"R1")==0){
		state=digitalRead(Rel1);
		fprintf(stdout, "%s" ,(state==0) ? "Off":"On" );
	}

	else if(strcmp(relayNumber,"R2")==0){
		state=digitalRead(Rel2);
		fprintf(stdout, "%s" ,(state==0) ? "Off":"On" );
	}

	else if(strcmp(relayNumber,"R3")==0){
		state=digitalRead(Rel3);
		fprintf(stdout, "%s" ,(state==0) ? "Off":"On" );
	}

	else if(strcmp(relayNumber,"R4")==0) {
		state=digitalRead(Rel4);
		fprintf(stdout, "%s" ,(state==0) ? "Off":"On" );

	}

	else if(strcmp(relayNumber,"R1-4")==0){
		int8_t states[NUMB_OF_RELAY];
		states[0]=digitalRead(Rel1);
		states[1]=digitalRead(Rel2);
		states[2]=digitalRead(Rel3);
		states[3]=digitalRead(Rel4);

		for (int8_t i=0; i<NUMB_OF_RELAY; i++){
			printf("%d",states[i]);
		}


	}


	else{
		fprintf(stderr,"Please specify a proper relay number!\n");
	}

	}


void pinsInit(void){
	wiringPiSetup();
	pinMode(Rel1,OUTPUT);
	pinMode(Rel2,OUTPUT);
	pinMode(Rel3,OUTPUT);
	pinMode(Rel4,OUTPUT);
	}


int main(int argc, char *argv[]){
	//fprintf(stdout,"OK\n\n");
	pinsInit();
	if(argc==3){
		char *cmd = argv[2];
		if(strcmp(cmd,"On")==0) energizingRelay(argv[1]);
		else if (strcmp(cmd,"Off")==0) deEnrgizeRelay(argv[1]);
		else if (strcmp(cmd,"State")==0) readingState(argv[1]);
		
		else{
			fprintf(stderr,
			"you should specify relay number and state e.g.\n"
			"./realy R1 Off\n./realy R1 On\n./realy R1 State\n");
			exit(EXIT_FAILURE);
		}
	}

	else {

		fprintf(stderr,
		"you should specify relay number and state e.g.\n"
		"./realy R1 Off\n./realy R1 On\n./realy R1 State\n");
		exit(EXIT_FAILURE);
	}

	exit(EXIT_SUCCESS);	

}
