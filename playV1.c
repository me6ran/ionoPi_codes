#include <syslog.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wiringPi.h>
#include <unistd.h>
#include <stdbool.h>


#define Rel1 0
#define Rel2 2
#define Rel3 3
#define Rel4 4



const char *logVoice1Info="Voice 1 has been played properly.";
const char *logVoice2Info="Voice 2 has been played properly.";
const char *logSbaInfo= "SBA has been played properly.";


void greet(void){
	//printf(%s,"OK");
	printf("Character is %s","OK");

}

void energizingRelay(char *relayNumber){
	if(strcmp(relayNumber,"R1")==0) digitalWrite(Rel1, HIGH);
	else if(strcmp(relayNumber,"R2")==0) digitalWrite(Rel2, HIGH);
	else if(strcmp(relayNumber,"R3")==0) digitalWrite(Rel3, HIGH);
	else if(strcmp(relayNumber,"R4")==0) digitalWrite(Rel4, HIGH);
	
	else{
		fprintf(stderr,"Please specify a proper relay number!\n");
	}
	
	
}

void deEnrgizeRelay(char *relayNumber){

	if(strcmp(relayNumber,"R1")==0) digitalWrite(Rel1, LOW);
	else if(strcmp(relayNumber,"R2")==0) digitalWrite(Rel2, LOW);
	else if(strcmp(relayNumber,"R3")==0) digitalWrite(Rel3, LOW);
	else if(strcmp(relayNumber,"R4")==0) digitalWrite(Rel4, LOW);
	
	else{
		fprintf(stderr,"Please specify a proper relay number!\n");
	}
	


}

void logging(char *logMsg, int LOGLevel){

	openlog("Play Voices", LOG_CONS | LOG_PID, LOG_USER);
	
	syslog(LOGLevel,"%s",logMsg);
	closelog();

	}

int isRelayOff(int relayNumber){
	sleep(1);
	fprintf(stderr,"The relay is not off, Trying to turn it off...\n");
	return digitalRead(relayNumber);
	}


int player(char *fileName ){
	char *fullAdrs=NULL;
	char *cmd=NULL;
	energizingRelay("R1"); //energizing relay 1 to turn the SpeedLight on
	energizingRelay("R2"); //energizing relay 1 to turn the Amplifier on
	char *preSiren="aplay -d 3 /home/pi/voices/Siren.wav";
	fullAdrs= malloc (16+strlen(fileName)+1); // put 1 at the end for null \0
	strcpy(fullAdrs,"/home/pi/voices/");
	strcat(fullAdrs,fileName);
	cmd = malloc(6+strlen(fullAdrs)+1);
	strcpy(cmd,"aplay ");
	strcat(cmd,fullAdrs);
	sleep(2);  // just having speedlight and amplifier on 2 seconds prior to playing sound
	system(preSiren);
	int ret=system(cmd);

	if(ret!=0){
		char *logMsg=NULL;
		logMsg = malloc (12+strlen(fullAdrs)+1);
		strcpy(logMsg,"Cannot Play ");
		strcat(logMsg, fileName);
		logging(logMsg,LOG_ERR);
		free(fullAdrs);
		free(cmd);
		free(logMsg);
		deEnrgizeRelay("R1");
		deEnrgizeRelay("R2");
		while(isRelayOff(Rel1)) deEnrgizeRelay("R1"); //to make sure the relay goes off
		while(isRelayOff(Rel2)) deEnrgizeRelay("R2");
		exit(EXIT_FAILURE);		
		}
	
	else{
		char *logMsg=NULL;
		logMsg = malloc (26+strlen(fullAdrs)+1);
		strcpy(logMsg, fileName);
		strcat(logMsg," has been played properly.");
		logging(logMsg,LOG_INFO);
		free(fullAdrs);
		free(cmd);
		free(logMsg);
		deEnrgizeRelay("R1");
		deEnrgizeRelay("R2");
		while(isRelayOff(Rel1)) deEnrgizeRelay("R1"); //to make sure the relay goes off
		while(isRelayOff(Rel2)) deEnrgizeRelay("R2");
		return EXIT_SUCCESS;
	}

}

void pinsInit(void){
		//fprintf(stdout,"Initializing outputs...");
		wiringPiSetup();
		pinMode(Rel1,OUTPUT);
		pinMode(Rel2,OUTPUT);
	}

int main (int argc, char *argv[]){
	//greet();
	pinsInit();
	if(argc==2){
		player(argv[1]);
		exit(EXIT_SUCCESS);
	}

	else {
		fprintf(stderr,"Wrong quantity of argument(s). given %d but expected 1.\n",argc-1);
		exit(EXIT_FAILURE);
	}
return EXIT_SUCCESS;
}
